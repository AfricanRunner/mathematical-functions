# Mathematical Functions

A simple program to parse String representations of functions and save them in a tree structure for evaluating the function at positions, calculating the derivative, and approximating integrals.

Example:

Function f = new Function("cos(x)"); // Creates a new function f.
	
double value = f.evaluate(3); // Evalutes the function at x = 3.
	
double slope = f.derivative(3); // Evalutes the derivative at x = 3.