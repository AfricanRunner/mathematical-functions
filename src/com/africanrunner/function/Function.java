package com.africanrunner.function;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import com.africanrunner.function.node.CommonConstant;
import com.africanrunner.function.node.CommonFunction;
import com.africanrunner.function.node.Constant;
import com.africanrunner.function.node.Node;
import com.africanrunner.function.node.Operator;
import com.africanrunner.function.node.Variable;

public class Function
{
	/** The head of the function tree. */
	private Node function;

	/**
	 * Default constructor for the <code>Function</code> class.
	 * 
	 * @param expression The function's expression.
	 */
	public Function(String expression)
	{
		Queue<String> split = splitFunction(expression);

		if (split.size() == 0)
			throw new InvalidFunctionException("Cannot create an empty function!");

		Node[] prefix = shuntingYard(split);

		function = prefix[prefix.length - 1];
		function.buildTree(prefix, prefix.length - 2);
	}

	/**
	 * Evaluates the function at <code>x</code>.
	 * 
	 * @param x The value to evaluate the function.
	 * @return The result of the evaluation.
	 */
	public double evaluate(double x)
	{
		return function.evaluate(x);
	}
	
	/**
	 * Calculates the derivative of the function at <code>x</code>.
	 * 
	 * @param x The value at which the derivative is calculated.
	 * @return The derivative of the function at <code>x</code>.
	 */
	public double derivative(double x)
	{
		return function.derivative(x);
	}

	/**
	 * Creates a <code>String</code> of the function in prefix notation.
	 * 
	 * @return Prefix representation of the function.
	 */
	public String prefix()
	{
		return function.prefix();
	}

	/**
	 * Creates a <code>String</code> of the function in infix notation.
	 * 
	 * @return Infix representation of the function.
	 */
	public String infix()
	{
		return function.infix(0);
	}

	/**
	 * Creates a <code>String</code> of the function in postfix notation.
	 * 
	 * @return Postfix representation of the function.
	 */
	public String postfix()
	{
		return function.postfix();
	}

	/**
	 * Creates a <code>String</code> of the function in infix notation.
	 * 
	 * @return Infix representation of the function.
	 */
	@Override
	public String toString()
	{
		return infix();
	}

	/**
	 * Adds whitespace padding around function elements to ensure a proper split.
	 * 
	 * @param raw The unpadded function.
	 * @return The function split into its components.
	 */
	public static Queue<String> splitFunction(String raw)
	{
		raw = raw.replaceAll("\\(", " ( ");
		raw = raw.replaceAll("\\)", " ) ");
		raw = raw.replaceAll("\\+", " + ");
		raw = raw.replaceAll("\\-", " - ");
		raw = raw.replaceAll("\\*", " * ");
		raw = raw.replaceAll("\\/", " / ");
		raw = raw.replaceAll("\\^", " ^ ");

		String[] elements = raw.trim().split("\\s+");
		Queue<String> infix = new LinkedList<String>();

		for (int i = 0; i < elements.length; i++)
		{
			if (elements[i].equals("-"))
			{
				if (i == 0 && elements.length > 1)
				{
					Node type = Node.parseNode(elements[i + 1]);
					if (type instanceof Constant)
						infix.offer("-" + elements[++i]);
					else
					{
						infix.offer("-1");
						infix.offer("*");
					}
				} else
				{
					Node before = Node.parseNode(elements[i - 1]);
					if (before instanceof Operator || elements[i - 1].matches("\\("))
					{
						Node type = Node.parseNode(elements[i + 1]);
						if (type instanceof Constant)
							infix.offer("-" + elements[++i]);
						else
						{
							infix.offer("-1");
							infix.offer("*");
						}
					} else
						infix.offer(elements[i]);
				}
			} else
				infix.offer(elements[i]);
		}

		return infix;
	}

	/**
	 * Generates an <code>Node</code> array in postfix notation from an array of raw
	 * <code>String</code> objects in infix notation. Uses the Shunting Yard
	 * algorithm.
	 * 
	 * @exception InvalidFunctionException Throws exception if <code>String</code>
	 *                                     elements do not match any valid
	 *                                     <code>Node</code> elements.
	 * 
	 * @param infix The raw input.
	 * @return The postfix array.
	 */
	private static Node[] shuntingYard(Queue<String> infix)
	{
		Queue<Node> postfix = new LinkedList<Node>();

		Stack<Stack<Node>> operators = new Stack<Stack<Node>>();
		operators.push(new Stack<Node>());

		for (String element : infix)
		{
			if (element.matches("\\("))
				operators.push(new Stack<Node>());
			else if (element.matches("\\)"))
			{
				Stack<Node> temp = operators.pop();
				while (!temp.isEmpty())
					postfix.offer(temp.pop());
			} else
			{
				Node current = Node.parseNode(element);

				if (current == null)
					throw new InvalidFunctionException("\"" + element + "\" is not a valid function element!");
				else if (current instanceof Constant || current instanceof Variable
						|| current instanceof CommonConstant)
					postfix.offer(current);
				else if (current instanceof CommonFunction)
					operators.peek().push(current);
				else if (current instanceof Operator)
				{
					while (!operators.isEmpty() && !operators.peek().isEmpty()
							&& ((operators.peek().peek() instanceof CommonFunction)
									|| (operators.peek().peek() instanceof Operator
											&& ((Operator) operators.peek().peek()).precedence() >= ((Operator) current)
													.precedence())))
						postfix.offer(operators.peek().pop());
					operators.peek().push(current);
				}
			}
		}

		while (!operators.isEmpty())
		{
			Stack<Node> temp = operators.pop();
			while (!temp.isEmpty())
				postfix.offer(temp.pop());
		}

		Node[] prefix = new Node[postfix.size()];
		for (int i = 0; i < prefix.length; i++)
			prefix[i] = postfix.poll();

		return prefix;
	}
}
