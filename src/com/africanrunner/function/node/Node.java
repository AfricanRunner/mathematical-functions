package com.africanrunner.function.node;

public abstract class Node
{
	/**
	 * Builds the function tree by linking the correct operators and functions with
	 * their child nodes.
	 * 
	 * @param prefix Array of nodes in prefix notation.
	 * @param index  The current index in the prefix array.
	 * @return The index after the current node has linked its children.
	 */
	public abstract int buildTree(Node[] prefix, int index);

	/**
	 * Evaluates the function at the value <code>x</code>.
	 * 
	 * @param x The value at which the function is evaluated.
	 * @return The value of the function at <code>x</code>.
	 */
	public abstract double evaluate(double x);
	
	/**
	 * Calculates the derivative of the function at the value <code>x</code>.
	 * 
	 * @param x The value at which the derivative is calculated.
	 * @return The derivative of the function at <code>x</code>.
	 */
	public abstract double derivative(double x);

	/**
	 * Builds a <code>String</code> representation of the function in prefix
	 * notation.
	 * 
	 * @return The prefix representation of the function.
	 */
	public abstract String prefix();
	
	/**
	 * Builds a <code>String</code> representation of the function in postfix
	 * notation.
	 * 
	 * @return The postfix representation of the function.
	 */
	public abstract String postfix();
	
	/**
	 * Builds a <code>String</code> representation of the function in infix
	 * notation.
	 * 
	 * @param precedence The current level of precedence. Used to ensure parenthesis
	 *                   are added when necessary.
	 * @return The infix representation of the function.
	 */
	public abstract String infix(int precedence);

	/**
	 * Creates a <code>Node</code> object based on a <code>String</code>
	 * representation of a function element. Returns <code>null</code> if the
	 * function element does not match any <code>Node</code> types.
	 * 
	 * @param raw The input function element.
	 * @return The <code>Node</code> equivalent of the input.
	 */
	public static Node parseNode(String raw)
	{
		Node value;

		value = Constant.parseConstant(raw);
		if (value != null)
			return value;

		value = Operator.parseOperator(raw);
		if (value != null)
			return value;

		value = Variable.parseVariable(raw);
		if (value != null)
			return value;

		value = CommonFunction.parseCommonFunction(raw);
		if (value != null)
			return value;

		value = CommonConstant.parseCommonConstant(raw);
		if (value != null)
			return value;

		return value;
	}
}
