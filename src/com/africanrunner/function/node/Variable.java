package com.africanrunner.function.node;

public class Variable extends Node
{
	/** The regex pattern for the <code>Variable</code> function element. */
	private static final String REGEX = "x";

	/** {@inheritDoc} */
	@Override
	public int buildTree(Node[] prefix, int index)
	{
		return index;
	}

	/** {@inheritDoc} */
	@Override
	public double evaluate(double x)
	{
		return x;
	}
	
	/** {@inheritDoc} */
	@Override
	public double derivative(double x)
	{
		return 1;
	}

	/** {@inheritDoc} */
	@Override
	public String infix(int precedence)
	{
		return "x";
	}

	/** {@inheritDoc} */
	@Override
	public String prefix()
	{
		return "x";
	}

	/** {@inheritDoc} */
	@Override
	public String postfix()
	{
		return "x";
	}

	/** {@inheritDoc} */
	@Override
	public String toString()
	{
		return "x";
	}

	/**
	 * Creates a <code>Variable</code> object based on a <code>String</code>
	 * representation of a function element. Returns <code>null</code> if the
	 * function element does not match the <code>REGEX</code> pattern.
	 * 
	 * @param raw The input function element.
	 * @return The <code>Variable</code> equivalent of the input.
	 */
	public static Variable parseVariable(String raw)
	{
		if (raw.matches(REGEX))
			return new Variable();

		return null;
	}
}
