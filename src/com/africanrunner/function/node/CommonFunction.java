package com.africanrunner.function.node;

import java.util.function.BiFunction;
import java.util.function.Function;

public class CommonFunction extends Node
{
	/** The function type of this object. */
	private CommonFunctionType function;

	/** The child of this function. */
	private Node child;

	/**
	 * Default constructor for <code>CommonFunction</code>.
	 * 
	 * @param function The type of function this <code>Node</code> is.
	 */
	public CommonFunction(CommonFunctionType function)
	{
		this.function = function;
	}

	/** {@inheritDoc} */
	@Override
	public int buildTree(Node[] prefix, int index)
	{
		child = prefix[index];
		index = prefix[index].buildTree(prefix, index - 1);

		return index;
	}

	/** {@inheritDoc} */
	@Override
	public double evaluate(double x)
	{
		return function.evaluate(child.evaluate(x));
	}
	
	/** {@inheritDoc} */
	@Override
	public double derivative(double x)
	{
		return function.derivative(child.evaluate(x), child.derivative(x));
	}

	/** {@inheritDoc} */
	@Override
	public String infix(int precedence)
	{
		return function.name().toLowerCase() + " (" + child.infix(0) + ")";
	}

	/** {@inheritDoc} */
	@Override
	public String prefix()
	{
		return function.name().toLowerCase() + " " + child.prefix();
	}

	/** {@inheritDoc} */
	@Override
	public String postfix()
	{
		return child.postfix() + " " + function.name().toLowerCase();
	}

	/** {@inheritDoc} */
	@Override
	public String toString()
	{
		return function.name().toLowerCase();
	}

	/**
	 * Creates a <code>CommonFunction</code> object based on a <code>String</code>
	 * representation of a function element. Returns <code>null</code> if the
	 * function element does not match any <code>CommonFunctionType</code>.
	 * 
	 * @param raw The input function element.
	 * @return The <code>CommonFunction</code> equivalent of the input.
	 */
	public static CommonFunction parseCommonFunction(String raw)
	{
		for (CommonFunctionType function : CommonFunctionType.values())
			if (raw.matches(function.regex()))
				return new CommonFunction(function);

		return null;
	}
}

/**
 * An <code>enum</code> to represent common functions.
 * 
 * @author Daniël du Preez
 *
 */
enum CommonFunctionType
{
	LN(t -> Math.log(t), 		(t, dt) -> dt / t), 
	LOG(t -> Math.log10(t), 	(t, dt) -> dt / (Math.log(10) * t)), 
	ABS(t -> Math.abs(t), 		(t, dt) -> Math.signum(t) * dt), 
	SIN(t -> Math.sin(t), 		(t, dt) -> Math.cos(t) * dt), 
	COS(t -> Math.cos(t), 		(t, dt) -> -Math.sin(t) * dt),
	TAN(t -> Math.tan(t), 		(t, dt) -> dt / (Math.cos(t) * Math.cos(t))), 
	CSC(t -> 1 / Math.sin(t), 	(t, dt) -> -dt * Math.cos(t) / (Math.sin(t) * Math.sin(t))), 
	SEC(t -> 1 / Math.cos(t), 	(t, dt) -> dt * Math.sin(t) / (Math.cos(t) * Math.cos(t))), 
	COT(t -> 1 / Math.tan(t), 	(t, dt) -> -dt / (Math.sin(t) * Math.sin(t))),
	SQRT(t -> Math.sqrt(t), 	(t, dt) -> dt / (2 * Math.sqrt(t))), 
	CBRT(t -> Math.cbrt(t), 	(t, dt) -> dt / (3 * Math.pow(t, 2.0/3.0))), 
	SINH(t -> Math.sinh(t), 	(t, dt) -> Math.cosh(t) * dt), 
	COSH(t -> Math.cosh(t), 	(t, dt) -> Math.sinh(t) * dt),
	TANH(t -> Math.tanh(t), 	(t, dt) -> dt / (Math.cosh(t) * Math.cosh(t))), 
	CSCH(t -> 1 / Math.sinh(t), (t, dt) -> -dt * Math.cosh(t) / (Math.sinh(t) * Math.sinh(t))), 
	SECH(t -> 1 / Math.cosh(t), (t, dt) -> -dt * Math.sinh(t) / (Math.cosh(t) * Math.cosh(t))), 
	COTH(t -> 1 / Math.tanh(t), (t, dt) -> -dt / (Math.sinh(t) * Math.sinh(t))),
	ARCSIN(t -> Math.asin(t), 	(t, dt) -> dt / (Math.sqrt(1 - t * t))), 
	ARCCOS(t -> Math.acos(t), 	(t, dt) -> -dt / (Math.sqrt(1 - t * t))), 
	ARCTAN(t -> Math.atan(t), 	(t, dt) -> dt / (t * t + 1)), 
	ARCCSC(t -> Math.asin(1 / t), (t, dt) -> -dt / (Math.abs(t) * Math.sqrt(t * t - 1))),
	ARCSEC(t -> Math.acos(1 / t), (t, dt) -> dt / (Math.abs(t) * Math.sqrt(t * t - 1))), 
	ARCCOT(t -> Math.atan(1 / t), (t, dt) -> -dt / (t * t + 1));

	/** The expression of the function. */
	private Function<Double, Double> expression;

	/** The derivative of the function. Given <code>t</code> and <code>dt</code>. */
	private BiFunction<Double, Double, Double> derivative;

	/**
	 * Default constructor for the <code>CommonFunctionType</code>.
	 * 
	 * @param expression An expression that takes a single <code>double</code> as an
	 *                   argument and returns a <code>double</code>.
	 */
	CommonFunctionType(Function<Double, Double> expression, BiFunction<Double, Double, Double> derivative)
	{
		this.expression = expression;
		this.derivative = derivative;
	}

	/**
	 * Evaluates the function at <code>t</code> based on the function's
	 * <code>expression</code>.
	 * 
	 * @param t The value to evaluate.
	 * @return The evaluation of the function at <code>t</code>.
	 */
	public double evaluate(double t)
	{
		return expression.apply(t);
	}
	
	/**
	 * Calculates the derivative of the function given <code>t</code> and <code>dt</code>.
	 * 
	 * @param t The value of the child function at a given <code>x</code>.
	 * @param dt The derivative of the child function at a given <code>x</code>.
	 * @return The derivative of parent function at a given <code>x</code>.
	 */
	public double derivative(double t, double dt)
	{
		return derivative.apply(t, dt);
	}

	/**
	 * Creates a regex pattern that represents the function type. Used to parse
	 * function types from a <code>String</code>.
	 * 
	 * @return Regex pattern of the function type.
	 */
	public String regex()
	{
		return this.name().toLowerCase();
	}
}
