package com.africanrunner.function.node;

public class CommonConstant extends Node
{
	/** Constant type of this object. */
	private CommonConstantType constant;

	/**
	 * Default constructor for <code>CommonConstant</code>.
	 * 
	 * @param constant The type of constant that this <code>Node</code>.
	 */
	public CommonConstant(CommonConstantType constant)
	{
		this.constant = constant;
	}

	/** {@inheritDoc} */
	@Override
	public int buildTree(Node[] prefix, int index)
	{
		return index;
	}

	/** {@inheritDoc} */
	@Override
	public double evaluate(double x)
	{
		return constant.getValue();
	}
	
	/** {@inheritDoc} */
	@Override
	public double derivative(double x)
	{
		return 0;
	}

	/** {@inheritDoc} */
	@Override
	public String infix(int precedence)
	{
		return constant.name().toLowerCase();
	}
	
	/** {@inheritDoc} */
	@Override
	public String prefix()
	{
		return constant.name().toLowerCase();
	}
	
	/** {@inheritDoc} */
	@Override
	public String postfix()
	{
		return constant.name().toLowerCase();
	}

	/** {@inheritDoc} */
	@Override
	public String toString()
	{
		return constant.name().toLowerCase();
	}

	/**
	 * Creates a <code>CommonConstant</code> object based on a <code>String</code>
	 * representation of a function element. Returns <code>null</code> if the
	 * function element does not match any <code>CommonConstantType</code>.
	 * 
	 * @param raw The input function element.
	 * @return The <code>CommonConstant</code> equivalent of the input.
	 */
	public static CommonConstant parseCommonConstant(String raw)
	{
		for (CommonConstantType constant : CommonConstantType.values())
			if (raw.matches(constant.regex()))
				return new CommonConstant(constant);

		return null;
	}
}

/**
 * An <code>enum</code> to represent common constants.
 * 
 * @author Daniël du Preez
 *
 */
enum CommonConstantType
{
	PI(Math.PI), E(Math.E);

	/** Value of the constant */
	private double value;

	/**
	 * Default constructor for the <code>CommonFunctionType</code>.
	 * 
	 * @param value The value of the constant.
	 */
	CommonConstantType(double value)
	{
		this.value = value;
	}

	/**
	 * Accessor method for the <code>value</code> variable.
	 * 
	 * @return The value of this constant.
	 */
	public double getValue()
	{
		return value;
	}

	/**
	 * Creates a regex pattern that represents the constant. Used to parse constant
	 * types from a <code>String</code>.
	 * 
	 * @return Regex pattern of the constant type.
	 */
	public String regex()
	{
		return this.name().toLowerCase();
	}
}
