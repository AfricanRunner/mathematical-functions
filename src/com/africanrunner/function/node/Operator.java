package com.africanrunner.function.node;

import java.util.function.BiFunction;

import com.africanrunner.function.InvalidFunctionException;

public class Operator extends Node
{
	/** Operator type of this object. */
	private OperatorType operator;

	/** The Operator's left child. */
	private Node leftChild;

	/** The Operator's right child. */
	private Node rightChild;

	/**
	 * Default constructor for <code>Operator</code>.
	 * 
	 * @param operator The type of operator that this <code>Node</code>.
	 */
	public Operator(OperatorType operator)
	{
		this.operator = operator;
	}

	/**
	 * Gets the precedence of the operator. Used to in the Shunting Yard algorithm
	 * to convert an expression from infix to postfix notation.
	 * 
	 * @return The precedence of this operator.
	 */
	public int precedence()
	{
		return operator.getPrecedence();
	}

	/** {@inheritDoc} */
	@Override
	public int buildTree(Node[] prefix, int index)
	{
		rightChild = prefix[index];
		index = prefix[index].buildTree(prefix, index - 1);

		leftChild = prefix[index];
		index = prefix[index].buildTree(prefix, index - 1);

		return index;
	}

	/** {@inheritDoc} */
	@Override
	public double evaluate(double x)
	{
		return operator.evaluate(leftChild.evaluate(x), rightChild.evaluate(x));
	}

	/** {@inheritDoc} */
	@Override
	public double derivative(double x)
	{
		return operator.derivative(leftChild.evaluate(x), leftChild.derivative(x), rightChild.evaluate(x),
				rightChild.derivative(x));
	}

	/** {@inheritDoc} */
	@Override
	public String infix(int precedence)
	{
		if (precedence > operator.getPrecedence())
			return "(" + leftChild.infix(operator.getPrecedence()) + " " + operator.getSymbol() + " "
					+ rightChild.infix(operator.getPrecedence()) + ")";
		else
			return leftChild.infix(operator.getPrecedence()) + " " + operator.getSymbol() + " "
					+ rightChild.infix(operator.getPrecedence());
	}

	/** {@inheritDoc} */
	@Override
	public String prefix()
	{
		return operator.getSymbol() + " " + leftChild.prefix() + " " + rightChild.prefix();
	}

	/** {@inheritDoc} */
	@Override
	public String postfix()
	{
		return leftChild.postfix() + " " + rightChild.postfix() + " " + operator.getSymbol();
	}

	/** {@inheritDoc} */
	@Override
	public String toString()
	{
		return operator.getSymbol();
	}

	/**
	 * Creates a <code>Operator</code> object based on a <code>String</code>
	 * representation of a function element. Returns <code>null</code> if the
	 * function element does not match any <code>OperatorType</code>.
	 * 
	 * @param raw The input function element.
	 * @return The <code>Operator</code> equivalent of the input.
	 */
	public static Operator parseOperator(String raw)
	{
		for (OperatorType operator : OperatorType.values())
			if (raw.matches(operator.regex()))
				return new Operator(operator);

		return null;
	}

}

/**
 * An <code>enum</code> to represent operator types.
 * 
 * @author danieldupreez
 *
 */
enum OperatorType
{
	ADD("+", 2, (t, u) -> t + u, 		(t, dt, u ,du) -> dt + du), 
	SUBTRACT("-", 2, (t, u) -> t - u, 	(t, dt, u, du) -> dt - du), 
	MULTIPLY("*", 3, (t, u) -> t * u, 	(t, dt, u, du) -> t * du + u * dt),
	DIVIDE("/", 3, (t, u) -> t / u, 	(t, dt, u, du) -> (u * dt - t * du) / (u * u)), 
	EXPONENT("^", 4, (t, u) -> Math.pow(t, u), (t, dt, u, du) -> Math.pow(t, u - 1) * (u * dt + t * nan(Math.log(t)) * du));

	/** The expression of the operator. */
	private BiFunction<Double, Double, Double> expression;

	/** The derivative of this operator. */
	private QuadFunction<Double, Double, Double, Double, Double> derivative;

	/** The operator's symbol. */
	private String symbol;

	/** The operator's precedence. */
	private int precedence;

	/**
	 * Default constructor for <code>OperatorType</code>.
	 * 
	 * @param symbol     The operator's symbol.
	 * @param precedence The operator's precedence.
	 * @param expression The operator's expression.
	 */
	OperatorType(String symbol, int precedence, BiFunction<Double, Double, Double> expression,
			QuadFunction<Double, Double, Double, Double, Double> derivative)
	{
		this.symbol = symbol;
		this.precedence = precedence;
		this.expression = expression;
		this.derivative = derivative;
	}

	/**
	 * Accessor method for the operator's symbol.
	 * 
	 * @return The operator's symbol.
	 */
	public String getSymbol()
	{
		return symbol;
	}

	/**
	 * Accessor method for the operator's precedence.
	 * 
	 * @return The operator's precedence.
	 */
	public int getPrecedence()
	{
		return precedence;
	}

	/**
	 * Evaluates the operator's expression given two arguments.
	 * 
	 * @param t The first argument.
	 * @param u The second argument.
	 * @return The result of the expression based on the two arguments.
	 */
	public double evaluate(double t, double u)
	{
		return expression.apply(t, u);
	}

	/**
	 * Evaluates the operator's derivative given two arguments and their derivative
	 * values.
	 * 
	 * @param t  The first argument.
	 * @param dt The derivative value of the first argument.
	 * @param u  The second argument.
	 * @param du The derivative value of the second argument.
	 * @return The derivative of the operator.
	 */
	public double derivative(double t, double dt, double u, double du)
	{
		return derivative.apply(t, dt, u, du);
	}

	/**
	 * Creates a regex pattern that represents the operator type. Used to parse
	 * operator types from a <code>String</code>.
	 * 
	 * @return Regex pattern of the operator type.
	 */
	public String regex()
	{
		return "\\" + symbol;
	}
	
	/**
	 * Takes a double and returns zero if the double is <code>NaN</code>.
	 * 
	 * @param a The double to be tested.
	 * @return A double that is zero instead of <code>NaN</code>.
	 */
	private static double nan(double a)
	{
		if(Double.isNaN(a))
			return 0;
		else
			return a;
	}
}

/**
 * An <code>interface</code> to represent a function with four arguments.
 * 
 * @author Daniël du Preez
 *
 * @param <T1> Argument type 1.
 * @param <T2> Argument type 2.
 * @param <T3> Argument type 3.
 * @param <T4> Argument type 4.
 * @param <T5> Argument type 5.
 */
interface QuadFunction<T1, T2, T3, T4, T5>
{
	/**
	 * Evaluate the function given four values.
	 * 
	 * @param t  Value 1.
	 * @param dt Value 2.
	 * @param u  Value 3.
	 * @param du Value 4.
	 * @return The result of the Overriden method.
	 */
	public T1 apply(T2 t, T3 dt, T4 u, T5 du);
}