package com.africanrunner.function.node;

public class Constant extends Node
{
	/** The regex pattern for the <code>Constant</code> function element. */
	private static final String REGEX = "\\-?\\d+(\\.\\d+)?";

	/** The value of the constant */
	private double value;

	/**
	 * Default constructor for a <code>Constant</code> object.
	 * 
	 * @param value The value of the constant.
	 */
	public Constant(double value)
	{
		this.value = value;
	}

	/** {@inheritDoc} */
	@Override
	public int buildTree(Node[] prefix, int index)
	{
		return index;
	}

	/** {@inheritDoc} */
	@Override
	public double evaluate(double x)
	{
		return value;
	}
	
	/** {@inheritDoc} */
	@Override
	public double derivative(double x)
	{
		return 0;
	}

	/** {@inheritDoc} */
	@Override
	public String infix(int precedence)
	{
		return "" + value;
		//return String.format("%.2f", value);
	}
	
	/** {@inheritDoc} */
	@Override
	public String prefix()
	{
		return "" + value;
		//return String.format("%.2f", value);
	}
	
	/** {@inheritDoc} */
	@Override
	public String postfix()
	{
		return "" + value;
		//return String.format("%.2f", value);
	}

	/** {@inheritDoc} */
	@Override
	public String toString()
	{
		return "" + value;
		//return String.format("%.2f", value);
	}

	/**
	 * Creates a <code>Constant</code> object based on a <code>String</code>
	 * representation of a function element. Returns <code>null</code> if the
	 * function element does not match the <code>REGEX</code> pattern.
	 * 
	 * @param raw The input function element.
	 * @return The <code>Constant</code> equivalent of the input.
	 */
	public static Constant parseConstant(String raw)
	{
		if (raw.matches(REGEX))
			return new Constant(Double.parseDouble(raw));

		return null;
	}
}