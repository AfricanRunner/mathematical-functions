package com.africanrunner.function;

public class InvalidFunctionException extends RuntimeException
{
	/**
	 * Creates a new <code>InvalidFunctionException</code> with an error message.
	 * 
	 * @param message The specifications of the error.
	 */
	public InvalidFunctionException(String message)
	{
		super(message);
	}
}
