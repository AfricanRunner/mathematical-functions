import com.africanrunner.function.Function;
import com.africanrunner.function.node.Variable;

public class Main
{
	public static void main(String[] args)
	{
		Function f = new Function("cos(x)");
		System.out.println(f.evaluate(2));
		System.out.println(f.derivative(2));
		
	}
}
